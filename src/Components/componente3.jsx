import React from 'react'
import banco from '../assets/img/portfolio/banco.png';
import compu from '../assets/img/portfolio/compu.png';
import escuela from '../assets/img/portfolio/escuela.png';


function componente3() {
    return (
        <div className="componente3">
            <section className='page-section fondo2 text-white mb-0' id="about">
                <div className='container'>
                    <div className='row'>
                        <div className='col'>
                            <h2 className='page-section-heading text-center text-uppercase text-black'>EXPERIENCIA</h2>
                            <div className='divider-custom divider-light'>
                                <div className='divider-custom-line'></div>
                                <div className='divider-custom-icon'><i className='fas fa-star'></i></div>
                                <div className='divider-custom-line'></div>
                            </div>
                            <div className='row'>
                                <div className='container1'>
                                    <div className='row'>
                                        <div className='col'>
                                            <img className='imgbanco' src={banco} alt="..." width="70" height="60" />
                                        </div>
                                        <div className='col'>
                                            <h1 className='navbar-brand banco text-black'>HACKATON BBVA</h1>
                                        </div>
                                        <div className='w-100'></div>
                                        <div className='col'>
                                            <img className='imgcompu' src={compu} alt="..." width="70" height="60" />
                                        </div>
                                        <div className='col'>
                                            <h1 className='navbar-brand compu text-black'>REALLY</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div className='col'>
                            <h2 className='page-section-heading text-center text-uppercase text-black'>FORMACIÓN</h2>
                            <div className='divider-custom divider-light'>
                                <div className='divider-custom-line'></div>
                                <div className='divider-custom-icon'><i className='fas fa-star'></i></div>
                                <div className='divider-custom-line'></div>
                            </div>
                            <br />
                            <div className='container2'>
                                <div className='row'>
                                    <div className='col'>
                                        <img className='imgescuela' src={escuela} alt="..." width="70" height="60" />
                                    </div>
                                    <div className='col'>
                                        <h1 className='navbar-brand teccu text-black' >TECNOLÓGICO DE CUAUTLA
                                        </h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>



        </div>
    )
}
export default componente3