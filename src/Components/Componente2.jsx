import avatar from '../assets/img/avataaars.svg';


function Componente2() {
    return (

        <div className="Componente2">

            <div className='masthead fondo text-white text-center'>
                <div className='container d-flex align-items-center flex-column'>
                    <img className='masthead-avatar mb-2' src={avatar} alt="..." width="100" height="100" />
                    <h1>Porcayo Espinoza Alicia</h1>
                    <div className='divider-custom divider-light'>
                        <div className='divider-custom-line'></div>
                        <div className='divider-custom-icon'><i class="fas fa-star"></i></div>
                        <div className='divider-custom-line'></div>
                    </div>
                    <p className='masthead-subheading font-weight-light mb-0'>ING. En Sistemas Computacionales</p>
                </div>
            </div>
        </div>

    )
}

export default Componente2

