import React from 'react'
import ciclismo from '../assets/img/portfolio/ciclismo.jpg';
import atardecer from '../assets/img/portfolio/atardecer.png';
import cine2 from '../assets/img/portfolio/cine2.jpg';
import deporte from '../assets/img/portfolio/deporte.png';
import moto from '../assets/img/portfolio/moto.png';
import viaje2 from '../assets/img/portfolio/viaje2.png';


function componente5() {
    return (
        <div className="componente5">

            <section className='fondo2'>
                <br />
                <div className='container'>
                    <h2 className='page-section-heading text-center text-uppercase text-black mb-0'>HOBBIE</h2>
                    <div className='divider-custom divider-light'>
                        <div className='divider-custom-line'></div>
                        <div className='divider-custom-icon'><i className='fas fa-star'></i></div>
                        <div className='divider-custom-line'></div>
                    </div>

                    <div className='row'>
                        <div className='col-6 col-md-4'>
                            <img className='img-fluid fw1' src={ciclismo} alt="..." width="80%" />
                        </div>
                        <div className='col-6 col-md-4'>

                            <img className='img-fluid fw2' src={atardecer} alt="..."
                                width="90%" height="80%" />
                        </div>
                        <div className='col-6 col-md-4'>
                            <img className='img-fluid fw3' src={cine2} alt="..." width="80%" height="6rem" />
                        </div>
                    </div>


                    <div className='row'>
                        <div className='col-6 col-md-4'>
                            <img className='img-fluid fw4' src={deporte} alt="..." width="80%" />
                        </div>
                        <div className='col-6 col-md-4'>

                            <img className='img-fluid fw5' src={moto} alt="..." width="90%" />
                        </div>
                        <div className='col-6 col-md-4'>
                            <img className='img-fluid fw6' src={viaje2} alt="..." width="80%" height="-20%" />
                        </div>
                    </div>
                </div>
                <br />
            </section>



        </div>
    )
}

export default componente5
