import cuautla from '../assets/img/cuautla.png';
import mex from '../assets/img/mex.png';



function EjemploComponent() {
  return (

    <div className="EjemploComponent">

      <div className="navbar bg-secondary text-uppercase fixed-top" id="mainNav">
        <div className="container1">
          <div className="row">
            <div className="col">
              <img className='imgtecnm' src={mex} alt="" width="80" height="60" />

            </div>
            <div className="col">
              <h1 className='navbar-brand h44 tecm'>Instituto Tecnológico de México</h1>
            </div>

          </div>
        </div>

        <div className="container1">
          <div className='row'>
            <div className='col'>
              <img src={cuautla} alt="" width="70" height="60" />

            </div>
            <div className='col'>
              <h1 className='navbar-brand text-center tecc' >Tecnológico de
                Cuautla</h1>
            </div>

          </div>
        </div>
      </div>



    </div>

  )
}

export default EjemploComponent