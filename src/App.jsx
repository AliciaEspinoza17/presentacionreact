import EjemploComponent from "./Components/EjemploComponent"
import Componente2 from "./Components/Componente2"
import Componente3 from "./Components/componente3"
import Componente4 from "./Components/componente4"
import Componente5 from "./Components/componente5"
import Componente6 from "./Components/componente6"


function App() {
  
  //LÓGICA  
  return (
    <>    
    <EjemploComponent />
    <Componente2 />
    <Componente3 />
    <Componente4 />
    <Componente5 />
    <Componente6 />
    </>
  )
}

export default App
